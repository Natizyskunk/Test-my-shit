$(function(){
	$('.nav-search-dropdown.searchSelect').click(function(e) {
    	e.stopPropagation();
  	});
});


// Getting selected value.
// elements: #table_id > thead > tr > th:nth-child(3) > select
$('select').val()


// Hiding select options
// elements: #table_id > thead > tr > th > select > option
$('select > option').prop('disabled', true);
// Add this to CSS
	/* select option[disabled] {
		display: none;
	} */
	
	
