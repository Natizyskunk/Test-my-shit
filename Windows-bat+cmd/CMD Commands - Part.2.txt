Commandes
Certaines commandes sont dangereuses (l'exemple de SYSKEY) et peuvent causer des problèmes pouvant conduire au formatage. D'autres commandes ne sont pas exécutables sous des versions antérieures de Windows.
Panneau de configuration
APPWIZ.CPL : ouvre l'outil Ajouter/Supprimer un programme
AZMAN.MSC : ouvre le gestionnaire d'autorisations (Pour Vista uniquement)
CERTMGR.MSC : ouvre les certificats pour l'utilisateur actuel
CLICONFG : ouvre la configuration des clients SQL
COMEXP.MSC ou bien DCOMCNFG : ouvre l'outil services et composants (Pour Vista uniquement)
COMPMGMT.MSC : ouvre l'outil de gestion de l'ordinateur
COMPUTERDEFAULTS : ouvrir l'outil des programmes par défaut (Pour Vista uniquement)
CONTROL /NAME MICROSOFT.BACKUPANDRESTORECENTER : ouvre le centre de sauvegarde et de restauration (Pour Vista uniquement). Tutoriel ICI
CONTROL ADMINTOOLS : ouvre les outils d'administrations
CONTROL COLOR : ouvre les paramètres de l'apparence
CONTROL FOLDERS : ouvre les options de dossiers
CONTROL FONTS : ouvre le gestionnaire de polices
CONTROL INTERNATIONAL ou bien INTL.CPL : ouvre les options régionales et linguistiques
CONTROL KEYBOARD : ouvre les propriétés du clavier
CONTROL MOUSE ou bien MAIN.CPL : ouvre les propriétés de la souris
CONTROL PRINTERS : ouvre les imprimantes et les fax disponibles
CONTROL USERPASSWORDS : ouvre l'éditeur des comptes utilisateurs
CONTROL USERPASSWORDS2 ou bien NETPLWIZ : contrôle les utilisateurs et leurs accès
CONTROL : ouvre le panneau de configuration
CREDWIZ : ouvre l'outil de sauvegarde et restauration des mots de passe des utilisateurs (Pour Vista uniquement)
DESK.CPL : ouvre les paramètres d'affichage
DEVMGMT.MSC : ouvre les gestionnaire de périphériques. Tutoriel ICI
DRWTSN32 : ouvre Dr. Watson (Pour XP uniquement)
DXDIAG : ouvre l'outil de diagnostic DirectX
EVENTVWR ou bien EVENTVWR.MSC : ouvre l'observateur d'évènements
FSMGMT.MSC : ouvre les dossiers partagés
GPEDIT.MSC : ouvre l'éditeur des stratégies de groupe (Pour les éditions professionnelles et plus de Windows)
HDWWIZ.CPL : ouvre l'assistant ajout de matériels
INFOCARDCPL.CPL : ouvre l'assistant compatibilité des programmes
IRPROPS.CPL : ouvre le gestionnaire d'infrarouge
ISCSICPL : ouvre l'outil de configuration de l'initiateur ISCI Microsoft (Pour Vista uniquement)
JOY.CPL : ouvre l'outil de contrôleur de jeu
LPKSETUP : ouvre l'assistant d'installation et désinstallation des langues d'affichage (Pour Vista uniquement)
LUSRMGR.MSC : ouvre l'éditeur des utilisateurs et groupes locaux
MDSCHED : ouvre l'outil de diagnostics de la mémoire Windows (Pour Vista uniquement)
MMC : ouvre une nouvelle console vide
MMSYS.CPL : ouvre les paramètres de sons
MOBSYNC : ouvre le centre de synchronisation
MSCONFIG : ouvre l'outil de configuration du système
NAPCLCFG.MSC : ouvre l'outil de configuration du client NAP (Pour Vista uniquement)
NTMSMGR.MSC : ouvre le gestionnaire des supports de stockage amovibles
NTMSOPRQ.MSC : ouvre les demandes de l'opérateur de stockage amovible
ODBCAD32 : ouvre l'administrateur de sources de données ODBC
OPTIONALFEATURES : ouvre l'outil Ajouter/Supprimer des composants Windows (Pour Vista uniquement)
PERFMON ou bien PERFMON.MSC : ouvre le moniteur de fiabilité et de performances Windows.
POWERCFG.CPL : ouvre le gestionnaire des modes d'alimentation (Pour Vista uniquement)
REGEDIT ou bien REGEDT32 (Pour Vista uniquement) : ouvre l'éditeur de registre
REKEYWIZ : ouvre le gestionnaire des certificats de chiffrement de fichiers (Pour Vista uniquement)
RSOP.MSC : ouvre le jeu de stratégie résultant
SECPOL.MSC : ouvre les paramètres de sécurités locales
SERVICES.MSC : ouvre le gestionnaire de services
SYSDM.CPL : ouvre les propriétés système
SYSEDIT : ouvre l'éditeur de configuration système (Attention, à manipuler avec prudence)
SYSKEY : ouvre l'utilitaire de protection de la base de données des comptes Windows (Attention, à manipuler avec extrême prudence !)
SYSPREP: ouvre le dossier contenant l'outil de préparation du système (Pour Vista uniquement)
TABLETPC.CPL : ouvre les paramètres pour Tablet pc (Pour Vista uniquement)
TASKSCHD.MSC ou bien CONTROL SCHEDTASKS : ouvre le planificateur de tâches (Pour Vista uniquement)
TELEPHON.CPL : ouvre l'outil de connexion téléphonique
TIMEDATE.CPL : ouvre les paramètres de l'heure et de la date
TPM.MSC : ouvre l'outil gestion de module de plateforme sécurisée sur l'ordinateur local (Pour Vista uniquement)
UTILMAN : ouvre les options d'ergonomie (Pour Vista uniquement)
VERIFIER : ouvre le gestionnaire de vérification des pilotes
WMIMGMT.MSC : ouvre Windows Management Infrastructure
WSCUI.CPL : ouvre le centre de sécurité Windows
WUAUCPL.CPL : ouvre le service de mise à jour Windows (Pour XP uniquement)

Programmes et outils Windows
%WINDIR%\SYSTEM32\RESTORE\RSTRUI.EXE : ouvre l'outil de restauration de système (Pour XP uniquement). Tutoriel ICI
CALC : ouvre la calculatrice
CHARMAP : ouvre la table des caractères
CLIPBRD : ouvre le presse papier (Pour XP uniquement, pour l'ajouter à Vista voir ICI)
CMD : ouvre l'invite de commandes
DIALER : ouvre le numérateur téléphonique de Windows
DVDPLAY : ouvre votre lecteur DVD
EUDCEDIT : ouvre l'éditeur de caractères privés
EXPLORER : ouvre l'explorateur Windows
FSQUIRT : Assistant transfert Bluetooth
IEXPLORE : ouvre Internet Explorer
IEXPRESS : ouvre l'assistant de création des archives auto-extractibles. Tutoriel ICI
JOURNAL : ouvre un nouveau journal (Pour Vista uniquement)
MAGNIFY : ouvre la loupe
MBLCTR : ouvre le centre de mobilité de Windows (Pour Vista uniquement)
MIGWIZ : ouvre l'outil de transfert de fichiers et de paramètres Windows (Pour Vista uniquement)
MIGWIZ.EXE : ouvre l'outil de transfert de fichiers et de paramètres Windows (pour XP uniquement)
MOVIEMK : ouvre Windows Movie Maker
MRT : lance l'utilitaire de suppression de logiciel malveillant. Tutoriel ICI
MSDT : ouvre l'outil de diagnostics et support Microsoft
MSINFO32 : ouvre les informations système
MSPAINT : ouvre Paint
MSRA : ouvre l'assistance à distance Windows
MSTSC : ouvre l'outil de connexion du bureau a distance
NOTEPAD : ouvre le bloc-notes
OSK : ouvre le clavier visuel. Tutoriel ICI
PRINTBRMUI : ouvre l'assistant de migration d'imprimante (Vista uniquement)
RSTRUI : ouvre l'outil de restauration du système (Pour Vista uniquement)
SIDEBAR : ouvre le volet Windows (Pour Vista uniquement)
SIGVERIF : ouvre l'outil de vérification des signatures de fichiers
SNDVOL : ouvre le mélangeur de volume
SNIPPINGTOOL : ouvre l'outil capture d'écran (Pour Vista uniquement). Tutoriel ICI
SOUNDRECORDER : ouvre le magnétophone
STIKYNOT : ouvre le pense-bête (Pour Vista uniquement)
TABTIP : ouvre le panneau de saisie Tablet PC (Pour Vista uniquement)
TASKMGR : ouvre le gestionnaire des tâches Windows
WAB : ouvre les contacts (Pour Vista uniquement)
WERCON : ouvre l'outil de rapports et de solutions aux problèmes (Pour Vista uniquement)
WINCAL : ouvre le calendrier Windows (Pour Vista uniquement)
WINCHAT : ouvre le logiciel Microsoft de chat en réseau (Pour Windows XP uniquement)
WINDOWSANYTIMEUPGRADE : permet la mise à niveau de Windows Vista
WINVER : ouvre la fenêtre pour connaître votre version Windows
WINWORD: ouvre Word (si il est installé)
WMPLAYER : ouvre le lecteur Windows Media
WRITE ou bien Wordpad : ouvre Wordpad

Gestion des disques
CHKDSK : effectue une analyse de la partition précisée dans les paramètres de la commande (Pour plus d'informations, tapez CHKDSK /? dans l'invite de commande CMD)
CLEANMGR : ouvre l'outil de nettoyage de disque
DEFRAG: Défragmente le disque dur (pour savoir comment utiliser, c'est ici )
DFRG.MSC : ouvre l'outil de défragmentation de disque
DISKMGMT.MSC : ouvre le gestionnaire de disques
DISKPART : ouvre l'outil de partitionnement (un peu lourd à manipuler)

Gestion des réseaux et Internet
CONTROL NETCONNECTIONS ou bien NCPA.CPL : ouvre les connexions réseau
FIREWALL.CPL : ouvre le pare-feu Windows
INETCPL.CPL : ouvre les propriétés internet
IPCONFIG : affiche les configurations des adresses IP sur l'ordinateur (Pour plus d'informations, tapez IPCONFIG /? dans l'invite de commande CMD)
NETSETUP.CPL : ouvre l'assistant configuration réseau (Pour XP uniquement)
WF.MSC : ouvre les fonctions avancées du pare-feu Windows (Pour Vista uniquement). Tutoriel ICI
À VOIR ÉGALEMENT : Commandes IP relatives aux réseaux sous Windows

Autres commandes
%HOMEDRIVE% : ouvre l'explorateur sur la partition ou le système d'exploitation est installé
%HOMEPATH% : ouvre le dossier d'utilisateur connecté actuellement C:\Documents and settings\[nom d'utilisateur]
%PROGRAMFILES% : ouvre le dossier d'installation d'autres programmes (Program Files)
%TEMP% ou bien %TMP% : ouvre le dossier temporaire
%USERPROFILE% : ouvre le dossier du profil de l'utilisateur connecté actuellement
%WINDIR% ou bien %SYSTEMROOT% : ouvre le dossier d'installation de Windows
%WINDIR%\system32\rundll32.exe shell32.dll,Control_RunDLL hotplug.dll : affiche la fenêtre "Supprimer le périphérique en toute sécurité"
AC3FILTER.CPL : ouvre les propriétés du filtre AC3 (Si installé)
FIREFOX : lance Mozilla FireFox (Si installé)
JAVAWS : Visualise le cache du logiciel JAVA (Si installé)
LOGOFF : ferme la session actuelle
NETPROJ : autorise ou pas la connexion à un projecteur réseau (Pour Vista uniquement)
Vérificateur des fichiers système (Nécessite un CD de Windows si le cache n'est pas disponible): (Tutoriel ICI)
SFC /SCANNOW : scanne immédiatement tous les fichiers système et répare les fichiers endommagés
SFC /VERIFYONLY : scanne seulement les fichiers système
SFC /SCANFILE="nom et chemin de fichier" : scanne le fichier précisé, et le répare s'il est endommagé
SFC /VERIFYFILE="nom et chemin de fichier" : scanne seulement le fichier précisé
SFC /SCANONCE : scanne les fichiers système au prochain redémarrage
SFC /REVERT : remet la configuration initiale (Pour plus d'informations, tapez SFC /? dans l'invite de commande CMD.
SHUTDOWN : éteint Windows
SHUTDOWN -A : interrompe l'arrêt de Windows
VSP1CLN : supprime le cache d'installation du service pack 1 de Vista 