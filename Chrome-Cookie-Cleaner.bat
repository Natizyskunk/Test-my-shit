@echo off
REM Delete all Chrome cookies and cookies related files.
taskkill /F /im chrome.exe
del /F /S /Q /A:R "%UserProfile%\AppData\Local\Google\Chrome\User Data\Default\*cookie*.*"
start chrome.exe
TIMEOUT /T 3 /NOBREAK
echo DONE !